package vip.xiaonuo.core.config;

import com.fhs.trans.cache.BothCacheService;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.noear.solon.data.cache.CacheService;
import org.noear.solon.data.cache.CacheServiceSupplier;

/**
 * @author noear 2023/1/14 created
 */
@Configuration
public class EasyTransConfigure {

    @Inject
    BothCacheService bothCacheService;

    @Bean
    public void initEasyTransCache(@Inject("${easy-trans.cache}") CacheServiceSupplier serviceSupplier) {
        CacheService cacheService = serviceSupplier.get();
        bothCacheService.setCacheService(cacheService);
    }
}
